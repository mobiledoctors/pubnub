var env = 'development_TruDoc';
var userId = 2;
var sessionId = new Date().getTime();
var channels = [
    env + '_calling_availability_' + userId,
];

var pubnub = new PubNub({
    subscribeKey: "sub-c-3ccb3b2a-b849-11e9-98d6-a6da8a14d776",
    publishKey: "pub-c-917430d1-9af9-43dd-9b0e-95b6645ec1fc",
    uuid: 2,
    logVerbosity: true,
    useInstanceId : true,
    // useInstanceId:2
});

window.addEventListener('offline', function () {
    window.addEventListener('online', function () {
        pubnub.subscribe({
            channels: [
                channels
            ],            
        });
    });
});
